package models;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LoginModel {

    private AccountModel account;
    private String userError;
    private String passwordError;
    private String emailError;
    private String passwordCondError;
    private String generalError;
    private String loginSuccess;

    public AccountModel getAccount() {
        return account;
    }

    @XmlElement
    public void setAccount(AccountModel account) {
        this.account = account;
    }

    public String getUserError() {
        return userError;
    }

    @XmlElement
    public void setUserError(String userError) {
        this.userError = userError;
    }

    public String getPasswordError() {
        return passwordError;
    }

    @XmlElement
    public void setPasswordError(String passwordError) {
        this.passwordError = passwordError;
    }

    public String getPasswordCondError() {
        return passwordCondError;
    }

    @XmlElement
    public void setPasswordCondError(String passwordCondError) {
        this.passwordCondError = passwordCondError;
    }

    public String getEmailError() {
        return emailError;
    }

    @XmlElement
    public void setEmailError(String emailError) {
        this.emailError = emailError;
    }

    public String getGeneralError() {
        return generalError;
    }

    @XmlElement
    public void setGeneralError(String generalError) {
        this.generalError = generalError;
    }

    public String getLoginSuccess() {
        return loginSuccess;
    }
    @XmlElement
    public void setLoginSuccess(String loginSuccess) {
        this.loginSuccess = loginSuccess;
    }

    public LoginModel() {
    }

    public LoginModel(String username, String password, String userError, String passwordError, String passwordCondError, String emailError, String generalError) {
        AccountModel ac = new AccountModel();
        ac.setUsername(username);
        ac.setPassword(password);
        this.account = ac;
        this.userError = userError;
        this.passwordError = passwordError;
        this.passwordCondError = passwordCondError;
        this.emailError = emailError;
        this.generalError = generalError;
    }
}
