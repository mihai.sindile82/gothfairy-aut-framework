package models;

public class ContactModel {

    private String name;
    private String email;
    private String message;
    private String fieldsError;
    private String emailError;
    private String contactSuccess;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFieldsError() {
        return fieldsError;
    }

    public void setFieldsError(String fieldsError) {
        this.fieldsError = fieldsError;
    }

    public String getEmailError() {
        return emailError;
    }

    public String getContactSuccess() {
        return contactSuccess;
    }

    public void setContactSuccess(String contactSuccess) {
        this.contactSuccess = contactSuccess;
    }

    public void setEmailError(String emailError) {
        this.emailError = emailError;
    }
}
