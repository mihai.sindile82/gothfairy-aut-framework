package models;

public class AccountModel {

    private String username;
    private String password;
    private String name;
    private Integer countryIndex;
    private Integer countyIndex;
    private String city;
    private String address;
    private String phone;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCountryIndex() {
        return countryIndex;
    }

    public void setCountryIndex(Integer countryIndex) {
        this.countryIndex = countryIndex;
    }

    public Integer getCountyIndex() {
        return countyIndex;
    }

    public void setCountyIndex(Integer countyIndex) {
        this.countyIndex = countyIndex;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
