package models;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class RegisterModel {

    private AccountModel account;
    private Integer isChecked;
    private String userError;
    private String passwordError;
    private String nameError;
    private String countryError;
    private String countyError;
    private String cityError;
    private String addressError;
    private String phoneError;
    private String emailError;
    private String phoneCondError;
    private String checkBoxError;
    private String passwordCondError;
    private String emailExistsError;
    private String registerSuccess;

    public AccountModel getAccount() {
        return account;
    }

    @XmlElement
    public void setAccount(AccountModel account) {
        this.account = account;
    }

    public String getUserError() {
        return userError;
    }

    @XmlElement
    public void setUserError(String userError) {
        this.userError = userError;
    }

    public String getPasswordError() {
        return passwordError;
    }

    @XmlElement
    public void setPasswordError(String passwordError) {
        this.passwordError = passwordError;
    }

    public String getNameError() {
        return nameError;
    }

    @XmlElement
    public void setNameError(String nameError) {
        this.nameError = nameError;
    }

    public String getCountryError() {
        return countryError;
    }

    @XmlElement
    public void setCountryError(String countryError) {
        this.countryError = countryError;
    }

    public String getCountyError() {
        return countyError;
    }

    @XmlElement
    public void setCountyError(String countyError) {
        this.countyError = countyError;
    }

    public String getCityError() {
        return cityError;
    }

    @XmlElement
    public void setCityError(String cityError) {
        this.cityError = cityError;
    }

    public String getAddressError() {
        return addressError;
    }

    @XmlElement
    public void setAddressError(String addressError) {
        this.addressError = addressError;
    }

    public String getPhoneError() {
        return phoneError;
    }

    @XmlElement
    public void setPhoneError(String phoneError) {
        this.phoneError = phoneError;
    }

    public String getEmailError() {
        return emailError;
    }

    @XmlElement
    public void setEmailError(String emailError) {
        this.emailError = emailError;
    }

    public String getPhoneCondError() {
        return phoneCondError;
    }

    @XmlElement
    public void setPhoneCondError(String phoneCondError) {
        this.phoneCondError = phoneCondError;
    }

    public String getCheckBoxError() {
        return checkBoxError;
    }

    @XmlElement
    public void setCheckBoxError(String checkBoxError) {
        this.checkBoxError = checkBoxError;
    }

    public Integer isChecked() {
        return isChecked;
    }

    @XmlElement
    public void setChecked(Integer checked) {
        isChecked = checked;
    }

    public String getPasswordCondError() {
        return passwordCondError;
    }

    @XmlElement
    public void setPasswordCondError(String passwordCondError) {
        this.passwordCondError = passwordCondError;
    }

    public String getEmailExistsError() {
        return emailExistsError;
    }

    @XmlElement
    public void setEmailExistsError(String emailExistsError) {
        this.emailExistsError = emailExistsError;
    }

    public String getRegisterSuccess() {
        return registerSuccess;
    }
    @XmlElement
    public void setRegisterSuccess(String registerSuccess) {
        this.registerSuccess = registerSuccess;
    }

    public RegisterModel() {
    }

    public RegisterModel(String username, String password, String userError, String passwordError, String nameError,
                         String countryError, String countyError, String cityError, String addressError,
                         String phoneError, String emailError, String phoneCondError, String checkBoxError,
                         String passwordCondError, String emailExistsError) {
        AccountModel ac = new AccountModel();
        ac.setUsername(username);
        ac.setPassword(password);
        this.account = ac;
        this.userError = userError;
        this.passwordError = passwordError;
        this.nameError = nameError;
        this.countryError = countryError;
        this.countyError = countyError;
        this.cityError = cityError;
        this.addressError = addressError;
        this.phoneError = phoneError;
        this.emailError = emailError;
        this.checkBoxError = checkBoxError;
        this.phoneCondError = phoneCondError;
        this.passwordCondError = passwordCondError;
        this.emailExistsError = emailExistsError;
    }
}
