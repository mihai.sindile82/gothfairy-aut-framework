package tests;

import com.opencsv.CSVReader;
import models.MainPageModel;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.MainPage;
import utils.Elements;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import static utils.SeleniumUtils.countElements;

public class MainPageTests extends BaseUITest{

    @DataProvider(name = "csvDp")
    public static Iterator<Object[]> csvDpCollection() throws IOException {
        Collection<Object[]> dp = new ArrayList<>();
        Path csvPath = Paths.get("src", "test", "resources", "data", "mainPageData.csv");
        File f = new File(csvPath.toString());
        Reader reader = Files.newBufferedReader(Paths.get(f.getAbsolutePath()));
        CSVReader csvReader = new CSVReader(reader);
        List<String[]> csvData = csvReader.readAll();
        for (int i = 0; i < csvData.size(); i++) {
            MainPageModel mpm = new MainPageModel();
            mpm.setProductNumber(Integer.parseInt(csvData.get(i)[0]));
            mpm.setPromoText(csvData.get(i)[1]);
            dp.add(new Object[]{mpm});
        }
        return dp.iterator();
    }

        public void mainPageCheck(MainPageModel mpm){
        MainPage mp = new MainPage(driver);
        mp.openMainPage(hostname);
        Integer i = countElements(driver, Elements.products);
        Assert.assertEquals(mp.promoElement(),mpm.getPromoText());
        Assert.assertEquals(mpm.getProductNumber(), i);
        Assert.assertTrue(mp.checkExistance());
    }

    @Test(dataProvider = "csvDp")
    public void checkMainPageElements(MainPageModel mpm) {
        test = extent.createTest("Check Elements on Main Page Test");
        mainPageCheck(mpm);
    }
}
