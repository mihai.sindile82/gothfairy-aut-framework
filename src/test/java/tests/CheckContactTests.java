package tests;

import models.ContactPageModel;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.CheckContactPage;
import utils.Elements;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import static utils.OtherUtils.sanitizeNullDbString;

public class CheckContactTests extends BaseUITest{

    @DataProvider(name = "sqlDp")
    public Iterator<Object[]> sqlDpCollection() {
        Collection<Object[]> dp = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(
                    "jdbc:mysql://" + dbHostname + ":" + dbPort + "/" + dbSchema,
                    dbUsername, dbPassword);
            Statement statement = connection.createStatement();
            ResultSet results = statement.executeQuery("SELECT * FROM automation.fairy_contact_info;");
            while (results.next()) {
                ContactPageModel cmp = new ContactPageModel();
                cmp.setName(sanitizeNullDbString(results.getString("name")));
                cmp.setInfo(sanitizeNullDbString(results.getString("info")));
                cmp.setAddress(sanitizeNullDbString(results.getString("address")));
                cmp.setEmail(sanitizeNullDbString(results.getString("email")));
                cmp.setPhone(sanitizeNullDbString(results.getString("phone")));
                dp.add(new Object[]{cmp});
            }
            statement.close();
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return dp.iterator();
    }

    private void contactWebpageCheck(ContactPageModel cpm) {
        CheckContactPage ccp = new CheckContactPage(driver);
        ccp.openContactPage(hostname);
        System.out.println("In DB:  " + cpm.getName());
        System.out.println("Online: " + ccp.contactFlow(Elements.firmName));
        System.out.println("In DB:  " + cpm.getInfo());
        System.out.println("Online: " + ccp.contactFlow(Elements.firmInfo));
        System.out.println("In DB:  " + cpm.getAddress());
        System.out.println("Online: " + ccp.contactFlow(Elements.firmAddress));
        System.out.println("In DB:  " + cpm.getEmail());
        System.out.println("Online: " + ccp.contactFlow(Elements.firmEmail));
        System.out.println("In DB:  " + cpm.getPhone());
        System.out.println("Online: " + ccp.contactFlow(Elements.firmPhone));
        Assert.assertEquals(cpm.getName(), ccp.contactFlow(Elements.firmName));
        Assert.assertEquals(cpm.getInfo(), ccp.contactFlow(Elements.firmInfo));
        Assert.assertEquals(cpm.getAddress(), ccp.contactFlow(Elements.firmAddress));
        Assert.assertEquals(cpm.getEmail(), ccp.contactFlow(Elements.firmEmail));
        Assert.assertEquals(cpm.getPhone(), ccp.contactFlow(Elements.firmPhone));
    }

    @Test(dataProvider = "sqlDp")
    public void checkContactInfo(ContactPageModel cpm){
        test = extent.createTest("Check Contact Info Test");
        contactWebpageCheck(cpm);
    }

}
