package tests;

import utils.Errors;
import utils.Notifications;
import com.opencsv.CSVReader;
import models.AccountModel;
import models.LoginModel;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.LoginPage;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import static utils.OtherUtils.sanitizeNullDbString;

public class LoginTests extends BaseUITest {

    @DataProvider(name = "sqlDp")
    public Iterator<Object[]> sqlDpCollection() {
        Collection<Object[]> dp = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(
                    "jdbc:mysql://" + dbHostname + ":" + dbPort + "/" + dbSchema, dbUsername, dbPassword);
            Statement statement = connection.createStatement();
            ResultSet results = statement.executeQuery("SELECT * FROM automation.fairy_auth;");
            while (results.next()) {
                AccountModel am = new AccountModel();
                am.setUsername(sanitizeNullDbString(results.getString("username")));
                am.setPassword(sanitizeNullDbString(results.getString("password")));
                LoginModel lm = new LoginModel();
                lm.setAccount(am);
                lm.setUserError(sanitizeNullDbString(results.getString("userError")));
                lm.setPasswordError(sanitizeNullDbString(results.getString("passwordError")));
                lm.setPasswordCondError(sanitizeNullDbString(results.getString("passwordCondError")));
                lm.setEmailError(sanitizeNullDbString(results.getString("emailError")));
                lm.setGeneralError(sanitizeNullDbString(results.getString("generalError")));
                dp.add(new Object[]{lm});
            }
            statement.close();
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return dp.iterator();
    }

    @DataProvider(name = "csvDp")
    public static Iterator<Object[]> csvDpCollection() throws IOException {
        Collection<Object[]> dp = new ArrayList<>();
        Path csvPath = Paths.get("src", "test", "resources", "data", "loginDataPositive.csv");
        File f = new File(csvPath.toString());
        Reader reader = Files.newBufferedReader(Paths.get(f.getAbsolutePath()));
        CSVReader csvReader = new CSVReader(reader);
        List<String[]> csvData = csvReader.readAll();
        for (int i = 0; i < csvData.size(); i++) {
            AccountModel am = new AccountModel();
            am.setUsername(csvData.get(i)[0]);
            am.setPassword(csvData.get(i)[1]);
            LoginModel lm = new LoginModel();
            lm.setAccount(am);
            lm.setLoginSuccess(csvData.get(i)[2]);
            dp.add(new Object[]{lm});
        }
        return dp.iterator();
    }

    private void loginActions(LoginModel lm) {
        LoginPage lp = new LoginPage(driver);
        lp.openLoginPage(hostname);
        lp.login(lm.getAccount().getUsername(), lm.getAccount().getPassword());
        Assert.assertTrue(lp.checkErr(lm.getUserError(), Errors.userError));
        Assert.assertTrue(lp.checkErr(lm.getPasswordError(), Errors.passwordError));
        Assert.assertTrue(lp.checkErr(lm.getPasswordCondError(), Errors.passwordCondError));
        Assert.assertTrue(lp.checkErr(lm.getEmailError(), Errors.emailError));
        Assert.assertTrue(lp.checkErr(lm.getGeneralError(), Errors.generalError));
    }

    private void loginActionsPositive(LoginModel lm) {
        LoginPage lp = new LoginPage(driver);
        lp.openLoginPage(hostname);
        lp.login(lm.getAccount().getUsername(), lm.getAccount().getPassword());
        Assert.assertTrue(lp.checkNotification(lm.getLoginSuccess(), Notifications.loginSuccess));
    }

    @Test(dataProvider = "sqlDp")
    public void loginNegativeTest(LoginModel lm) {
        test = extent.createTest("Negative Login Test");
        loginActions(lm);
        extent.flush();
    }

    @Test(dataProvider = "csvDp")
    public void loginPositiveTest(LoginModel lm) {
        test = extent.createTest("Positive Login Test");
        loginActionsPositive(lm);
    }
}
