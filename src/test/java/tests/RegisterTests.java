package tests;

import utils.Errors;
import utils.Notifications;
import com.opencsv.CSVReader;
import models.AccountModel;
import models.RegisterModel;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.RegisterPage;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import static utils.OtherUtils.sanitizeNullDbInt;
import static utils.OtherUtils.sanitizeNullDbString;

public class RegisterTests extends BaseUITest {

    @DataProvider(name = "sqlDp")
    public Iterator<Object[]> sqlDpCollection() {
        Collection<Object[]> dp = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(
                    "jdbc:mysql://" + dbHostname + ":" + dbPort + "/" + dbSchema,
                    dbUsername, dbPassword);
            Statement statement = connection.createStatement();
            ResultSet results = statement.executeQuery("SELECT * FROM automation.fairy_reg;");
            while (results.next()) {
                AccountModel am = new AccountModel();
                am.setUsername(sanitizeNullDbString(results.getString("username")));
                am.setPassword(sanitizeNullDbString(results.getString("password")));
                am.setName(sanitizeNullDbString(results.getString("name")));
                am.setCountryIndex(sanitizeNullDbInt(results.getInt("countryIndex")));
                am.setCountyIndex(sanitizeNullDbInt(results.getInt("countyIndex")));
                am.setCity(sanitizeNullDbString(results.getString("city")));
                am.setAddress(sanitizeNullDbString(results.getString("address")));
                am.setPhone(sanitizeNullDbString(results.getString("telephone")));

                RegisterModel rm = new RegisterModel();
                rm.setAccount(am);
                rm.setChecked(sanitizeNullDbInt(results.getInt("isChecked")));
                rm.setUserError(sanitizeNullDbString(results.getString("userError")));
                rm.setPasswordError(sanitizeNullDbString(results.getString("passwordError")));
                rm.setNameError(sanitizeNullDbString(results.getString("nameError")));
                rm.setCountryError(sanitizeNullDbString(results.getString("countryError")));
                rm.setCountyError(sanitizeNullDbString(results.getString("countyError")));
                rm.setCityError(sanitizeNullDbString(results.getString("cityError")));
                rm.setAddressError(sanitizeNullDbString(results.getString("addressError")));
                rm.setPhoneError(sanitizeNullDbString(results.getString("phoneError")));
                rm.setEmailError(sanitizeNullDbString(results.getString("emailError")));
                rm.setPhoneCondError(sanitizeNullDbString(results.getString("phoneCondError")));
                rm.setCheckBoxError(sanitizeNullDbString(results.getString("checkBoxError")));
                rm.setPasswordCondError(sanitizeNullDbString(results.getString("passwordCondError")));
                rm.setEmailExistsError(sanitizeNullDbString(results.getString("emailExistsError")));
                dp.add(new Object[]{rm});
            }
            statement.close();
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return dp.iterator();
    }

    @DataProvider(name = "csvDp")
    public Iterator<Object[]> csvDpCollection() throws IOException {
        Collection<Object[]> dp = new ArrayList<>();
        Path csvPath = Paths.get("src", "test", "resources", "data", "registerDataPositive.csv");
        File f = new File(csvPath.toString());
        Reader reader = Files.newBufferedReader(Paths.get(f.getAbsolutePath()));
        CSVReader csvReader = new CSVReader(reader);
        List<String[]> csvData = csvReader.readAll();
        for (int i = 0; i < csvData.size(); i++) {
            AccountModel am = new AccountModel();
            am.setUsername(csvData.get(i)[0]);
            am.setPassword(csvData.get(i)[1]);
            am.setName(csvData.get(i)[2]);
            am.setCountryIndex(Integer.parseInt(csvData.get(i)[3]));
            am.setCountyIndex(Integer.parseInt(csvData.get(i)[4]));
            am.setCity(csvData.get(i)[5]);
            am.setAddress(csvData.get(i)[6]);
            am.setPhone(csvData.get(i)[8]);
            RegisterModel rm = new RegisterModel();
            rm.setAccount(am);
            rm.setChecked(Integer.parseInt(csvData.get(i)[7]));
            rm.setRegisterSuccess(csvData.get(i)[9]);
            dp.add(new Object[]{rm});
        }
        return dp.iterator();
    }

    private void registerActions(RegisterModel rm) {
        RegisterPage rp = new RegisterPage(driver);
        rp.openRegisterPage(hostname);
        rp.register(rm.getAccount().getUsername(), rm.getAccount().getPassword(), rm.getAccount().getName(),
                rm.getAccount().getCountryIndex(), rm.getAccount().getCountyIndex(), rm.getAccount().getCity(),
                rm.getAccount().getAddress(), rm.isChecked(), rm.getAccount().getPhone());
        Assert.assertTrue(rp.checkErr(rm.getUserError(), Errors.userError));
        Assert.assertTrue(rp.checkErr(rm.getPasswordError(), Errors.passwordError));
        Assert.assertTrue(rp.checkErr(rm.getNameError(), Errors.nameError));
        Assert.assertTrue(rp.checkErr(rm.getCountryError(), Errors.countryError));
        Assert.assertTrue(rp.checkErr(rm.getCountyError(), Errors.countyError));
        Assert.assertTrue(rp.checkErr(rm.getCityError(), Errors.cityError));
        Assert.assertTrue(rp.checkErr(rm.getAddressError(), Errors.addressError));
        Assert.assertTrue(rp.checkErr(rm.getPhoneError(), Errors.phoneError));
        Assert.assertTrue(rp.checkErr(rm.getEmailError(), Errors.emailError));
        Assert.assertTrue(rp.checkErr(rm.getPhoneCondError(), Errors.phoneCondError));
        Assert.assertTrue(rp.checkErr(rm.getCheckBoxError(), Errors.checkBoxError));
        Assert.assertTrue(rp.checkErr(rm.getPasswordCondError(), Errors.passwordCondError));
        Assert.assertTrue(rp.checkErr(rm.getEmailExistsError(), Errors.emailExistsError));
    }

    private void registerActionsPositive(RegisterModel rm) {
        RegisterPage rp = new RegisterPage(driver);
        rp.openRegisterPage(hostname);
        rp.register(rm.getAccount().getUsername(), rm.getAccount().getPassword(), rm.getAccount().getName(),
                rm.getAccount().getCountryIndex(), rm.getAccount().getCountyIndex(), rm.getAccount().getCity(),
                rm.getAccount().getAddress(), rm.isChecked(), rm.getAccount().getPhone());
        Assert.assertTrue(rp.checkNotification(rm.getRegisterSuccess(), Notifications.registrationSuccess));
    }

    @Test(dataProvider = "sqlDp")
    public void registerNegativeTest(RegisterModel rm) {
        test = extent.createTest("Negative Registration Test");
        registerActions(rm);
    }

    @Test(dataProvider = "csvDp")
    public void registerPositiveTest(RegisterModel rm) {
        test = extent.createTest("Positive Registration Test");
        registerActionsPositive(rm);
    }

}
