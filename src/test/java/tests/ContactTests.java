package tests;

import utils.Errors;
import utils.Notifications;
import com.opencsv.CSVReader;
import models.ContactModel;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.ContactFormPage;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import static utils.OtherUtils.sanitizeNullDbString;

public class ContactTests extends  BaseUITest{

    @DataProvider(name = "sqlDp")
    public Iterator<Object[]> sqlDpCollection() {
        Collection<Object[]> dp = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(
                    "jdbc:mysql://" + dbHostname + ":" + dbPort + "/" + dbSchema,
                    dbUsername, dbPassword);
            Statement statement = connection.createStatement();
            ResultSet results = statement.executeQuery("SELECT * FROM automation.fairy_contact;");
            while (results.next()) {
                ContactModel cm = new ContactModel();
                cm.setName(sanitizeNullDbString(results.getString("name")));
                cm.setMessage(sanitizeNullDbString(results.getString("message")));
                cm.setEmail(sanitizeNullDbString(results.getString("email")));
                cm.setFieldsError(sanitizeNullDbString(results.getString("fieldsError")));
                cm.setEmailError(sanitizeNullDbString(results.getString("emailError")));
                dp.add(new Object[]{cm});
            }
            statement.close();
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return dp.iterator();
    }

    @DataProvider(name = "csvDp")
    public Iterator<Object[]> csvDpCollection() throws IOException {
        Collection<Object[]> dp = new ArrayList<>();
        Path csvPath = Paths.get("src", "test", "resources", "data", "ContactDataPositive.csv");
        File f = new File(csvPath.toString());
        Reader reader = Files.newBufferedReader(Paths.get(f.getAbsolutePath()));
        CSVReader csvReader = new CSVReader(reader);
        List<String[]> csvData = csvReader.readAll();
        for (int i = 0; i < csvData.size(); i++) {
            ContactModel cm = new ContactModel();
            cm.setName(csvData.get(i)[0]);
            cm.setEmail(csvData.get(i)[1]);
            cm.setMessage(csvData.get(i)[2]);
            cm.setContactSuccess(csvData.get(i)[3]);
            dp.add(new Object[]{cm});
        }
        return dp.iterator();
    }

    private void contactActions(ContactModel cm) {
        ContactFormPage cp = new ContactFormPage(driver);
        driver.get(hostname);
        cp.contactForm(cm.getName(), cm.getEmail(), cm.getMessage());
        Assert.assertTrue(cp.checkErr(cm.getFieldsError(), Errors.fieldsError));
        Assert.assertTrue(cp.checkErr(cm.getEmailError(), Errors.emailError));
        cp.clearContactForm();
    }

    private void contactPositiveActions(ContactModel cm) {
        ContactFormPage cp = new ContactFormPage(driver);
        cp.openContactPage(hostname);
        cp.contactForm(cm.getName(), cm.getEmail(), cm.getMessage());
        Assert.assertTrue(cp.checkNotification(cm.getContactSuccess(), Notifications.contactSuccess));
    }

    @Test(dataProvider = "sqlDp")
    public void contactNegativeTest(ContactModel cm) {
        test = extent.createTest("Negative Contact Messenger Test");
        contactActions(cm);
    }

    @Test(dataProvider = "csvDp")
    public void contactPositiveTest(ContactModel cm) {
        test = extent.createTest("Positive Contact Messenger Test");
        contactPositiveActions(cm);
    }

}
