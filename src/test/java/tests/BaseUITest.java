package tests;

import com.aventstack.extentreports.reporter.JsonFormatter;
import org.testng.annotations.AfterClass;
import utils.SeleniumUtils;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Properties;

public class BaseUITest {
    WebDriver driver;
    String hostname;
    String hostpage;
    String dbHostname;
    String dbPort;
    String dbSchema;
    String dbUsername;
    String dbPassword;
    String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
    ExtentSparkReporter spark = new ExtentSparkReporter("test-results/ExtendReports/SuiteResults"  + timeStamp + ".html");
    JsonFormatter json = new JsonFormatter("extent.json");
    ExtentReports extent = new ExtentReports();
    ExtentTest test;

    @BeforeClass
    public void setUp() {
        String browserType;
        //Method 1 -D cmd line parameters
        System.out.println(System.getProperty("browser"));
        browserType = System.getProperty("browser");

        try {
            //read from properties file
            InputStream input = new FileInputStream("src\\test\\java\\framework.properties");
            Properties prop = new Properties();
            prop.load(input);

//            read default value from config
            if (browserType == null)
                browserType = prop.getProperty("browser");

            System.out.println("Run test with browser:" + browserType);
            driver = SeleniumUtils.getDriver(browserType);

            hostname = prop.getProperty("hostname");
            hostpage = prop.getProperty("hostpage");
            System.out.println("Use the next hostname:" + hostname);
            dbHostname = prop.getProperty("dbHostname");
            System.out.println("Using DB hostname: " + dbHostname);
            dbPort = prop.getProperty("dbPort");
            System.out.println("Using DB port: " + dbPort);
            dbSchema = prop.getProperty("dbSchema");
            System.out.println("Using DB Schema: " + dbSchema);
            dbUsername = prop.getProperty("dbUser");
            dbPassword = prop.getProperty("dbPassword");
            System.out.println("Using DB credentials: " + dbUsername + "/" + dbPassword);

//            setUp expected reports
            extent.createDomainFromJsonArchive("extent.json");
            extent.attachReporter(json, spark);
            extent.attachReporter(spark);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @AfterClass
    public void cleanUp() {
        System.out.println("Close driver at end of class test");
        driver.quit();
    }

    @AfterMethod
    public synchronized void afterMethod(ITestResult result) {
        if (test != null) {
            switch (result.getStatus()) {
                case ITestResult.FAILURE:
                    String errorMsg = result.getThrowable().getMessage();
                    String base64Screenshot = "data:image/png;base64," + ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64);
                    test.fail("TEST FAILED:" + errorMsg, MediaEntityBuilder.createScreenCaptureFromBase64String(base64Screenshot).build());
                    break;
                case ITestResult.SUCCESS:
                    test.log(Status.PASS, "TEST PASSED");
                    break;
                case ITestResult.SKIP:
                    test.log(Status.SKIP, "TEST SKIPP");
            }
            extent.flush();
        }
    }

    public void logInfoStatus(String message) {
        if (test != null)
            test.log(Status.INFO, message);
        System.out.println(message);
    }
}
