package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjects.BrandPage;
import utils.Elements;
import utils.Filters;

import static utils.SeleniumUtils.countElements;

public class BrandTests extends BaseUITest{

    private void brandActions(Filters brand) {
        BrandPage bp = new BrandPage(driver);
        bp.openBrandPage(hostname);
        int countExpected = bp.brandClickFilter(brand);
        Assert.assertEquals(countExpected, countElements(driver, Elements.generic));
        System.out.println("Nr of products: " + countElements(driver, Elements.generic));
    }

    @Test
    public void brandTestSW() {
        test = extent.createTest("Star Wars Brand filter Test");
        brandActions(Filters.StarWars);
        System.out.println(driver.getCurrentUrl());
    }

    @Test
    public void brandTestMarvel() {
        test = extent.createTest("Marvel Brand filter Test");
        brandActions(Filters.Marvel);
        System.out.println(driver.getCurrentUrl());
    }

}
