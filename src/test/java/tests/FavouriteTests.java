package tests;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.FavouritePage;
import utils.Elements;

import static utils.SeleniumUtils.countElements;

public class FavouriteTests extends BaseUITest {

    private void favouriteActions(){
        FavouritePage fp = new FavouritePage(driver);
        fp.openCelticPage(hostname, hostpage);
        fp.addToFavourite();
        int i = countElements(driver, Elements.products);
        Assert.assertEquals(i, 1);
        System.out.println(i);
    }

    @BeforeMethod
    public void emptyFavList(){
        FavouritePage fp = new FavouritePage(driver);
        driver.get(hostname + "catalog/favorite");
        if(countElements(driver, Elements.products)>0)
            fp.removeFavouriteFlow();
    }

    @Test
    public void favouriteCheck() {
        test = extent.createTest("Check Favourite product is added Test");
        favouriteActions();
    }

}
