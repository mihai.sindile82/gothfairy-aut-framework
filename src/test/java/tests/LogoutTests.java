package tests;

import models.LoginModel;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjects.LoginPage;
import pageObjects.ProfilePage;

public class LogoutTests extends BaseUITest{

    public void logoutActions(LoginModel lm) {
        LoginPage lp = new LoginPage(driver);
        lp.openLoginPage(hostname);
        lp.login(lm.getAccount().getUsername(), lm.getAccount().getPassword());
        ProfilePage pp = new ProfilePage(driver);
        pp.logoutAction();
        Assert.assertTrue(pp.logout());
        driver.navigate().back();
        Assert.assertTrue(pp.logout());
    }

    @Test(dataProviderClass = LoginTests.class, dataProvider = "csvDp")
    public void logoutCheck(LoginModel lm){
        test = extent.createTest("Logout Check Test");
        logoutActions(lm);
    }

}
