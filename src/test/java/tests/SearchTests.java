package tests;

import com.opencsv.CSVReader;
import models.SearchModel;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.SearchPage;
import utils.Elements;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import static utils.SeleniumUtils.countElements;

public class SearchTests extends BaseUITest{

    @DataProvider(name = "csvDp")
    public static Iterator<Object[]> csvDpCollection() throws IOException {
        Collection<Object[]> dp = new ArrayList<>();
        Path csvPath = Paths.get("src", "test", "resources", "data", "searchData.csv");
        File f = new File(csvPath.toString());
        Reader reader = Files.newBufferedReader(Paths.get(f.getAbsolutePath()));
        CSVReader csvReader = new CSVReader(reader);
        List<String[]> csvData = csvReader.readAll();
        for (int i = 0; i < csvData.size(); i++) {
            SearchModel sm = new SearchModel();
            sm.setSearchCriteria(csvData.get(i)[0]);
            sm.setProductNumber(Integer.parseInt(csvData.get(i)[1]));
            dp.add(new Object[]{sm});
        }
        return dp.iterator();
    }

    public void searchCheck(SearchModel sm){
        SearchPage sp = new SearchPage(driver);
        sp.openMainPage(hostname);
        sp.register(sm.getSearchCriteria());
        Integer i = countElements(driver, Elements.products);
        Assert.assertEquals(sm.getProductNumber(), i);
    }

    @Test(dataProvider = "csvDp")
    public void checkSearchResults(SearchModel sm) {
        test = extent.createTest("Search Test");
        searchCheck(sm);
    }

}
