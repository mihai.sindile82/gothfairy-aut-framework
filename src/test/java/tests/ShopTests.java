package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjects.ShopPage;

public class ShopTests extends BaseUITest{

    private void shopVerifyProductPrice(){
        ShopPage sp = new ShopPage(driver);
        sp.openCelticPage(hostname, hostpage);
        sp.goToProduct();
        float initial = sp.productPriceInitial();
        System.out.println("Product price on page: " + initial);
        sp.addToCart();
        System.out.println("Product price in cart: " + sp.productPriceInCart());
        Assert.assertEquals(initial, sp.productPriceInCart());
        sp.goToCheckOut();
        Assert.assertEquals(initial, sp.productPriceInCheckout());
        Assert.assertEquals(sp.totalPrice(),sp.productPriceInCheckout() + sp.shippingPrice());
    }

    private void shopVerifyAddProductByButtons(){
        ShopPage sp = new ShopPage(driver);
        sp.openCelticPage(hostname, hostpage);
        sp.goToProduct();
        float initial = sp.productPriceInitial();
        sp.addToCart();
        sp.addPlusProduct();
        float fin = initial * sp.numberOfProducts();
        Assert.assertEquals(fin, sp.productPriceInCart());
    }

    private void shopVerifyRemoveProductByButtons(){
        ShopPage sp = new ShopPage(driver);
        sp.openCelticPage(hostname, hostpage);
        sp.goToProduct();
        sp.addToCart();
        sp.removeMinusProduct();
        Assert.assertTrue(sp.checkNoItems());
    }

    @Test
    public void verifyPrice(){
        test = extent.createTest("Verify Price of Product page/cart/checkout/total Test");
        shopVerifyProductPrice();
        extent.flush();
    }

    @Test
    public void verifyPriceAddButtons(){
        test = extent.createTest("Verify add product Test");
        shopVerifyAddProductByButtons();
    }

    @Test
    public void verifyRemoveOfProduct(){
        test = extent.createTest("Verify remove of product Test");
        shopVerifyRemoveProductByButtons();
    }

}
