package utils;

public enum Elements {
    firmName,
    firmInfo,
    firmAddress,
    firmEmail,
    firmPhone,
    generic,
    products
}
