package utils;

import org.openqa.selenium.*;

public class OtherUtils {

    public static String sanitizeNullDbString(String dbResult) {
        if (dbResult == null) {
            return "";
        }
        return dbResult;
    }

    public static int sanitizeNullDbInt(Integer dbResult) {
        if (dbResult == null) {
            return 0;
        }
        return dbResult;
    }

    public static boolean checkMessagePresentOnElement(WebElement element, String expectedError) {
        try {
            String actualMsg = element.getText();
            return expectedError.equals(actualMsg);
        } catch (NoSuchElementException e) {
            if (expectedError.isEmpty())
                return true;
        }
        return false;
    }

    public static boolean checkElementPresent(WebElement element) {
        try {
            return true;
        } catch (NoSuchElementException e) {
                return false;
        }
    }

    //for some reason, this doesn't work. but if i call JavascriptExecutor at the spot i need, it works
    public static void jsExecute(WebDriver driver, WebElement element){
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click", element);
    }

    public static float priceConverter(String full){
        String trim = full.substring(0, full.indexOf(" ")).replace(",", ".");
        float price = Float.parseFloat(trim);
        return price;
    }

    public static boolean retryingFindClick(WebElement webElement) {
        boolean result = false;
        int attempts = 0;
        while(attempts < 5) {
            try {
                webElement.click();
                result = true;
                break;
            } catch(StaleElementReferenceException e) {
            }
            attempts++;
        }
        return result;
    }

}
