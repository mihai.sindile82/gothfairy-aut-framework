package utils;

public enum Errors {
    userError,
    passwordError,
    passwordCondError,
    emailError,
    generalError,
    nameError,
    countryError,
    countyError,
    cityError,
    addressError,
    phoneError,
    checkBoxError,
    phoneCondError,
    fieldsError,
    emailExistsError
}
