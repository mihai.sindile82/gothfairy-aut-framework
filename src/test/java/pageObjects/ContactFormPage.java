package pageObjects;

import utils.Errors;
import utils.Notifications;
import utils.OtherUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ContactFormPage {

    private WebDriver driver;
    WebDriverWait wait;


    @FindBy(how = How.CSS, using = "#olark-wrapper > div.olark-launch-button-wrapper.olark-text-button > div > button")
    WebElement contactButton;

    @FindBy(how = How.CSS, using = "#olark-custom-survey-form > div > div > div.olark-survey-form-item.olark-survey-form-item-name.olark-survey-form-is-required > input")
    WebElement nameInput;

    @FindBy(how = How.CSS, using = "#olark-custom-survey-form > div > div > div.olark-survey-form-item.olark-survey-form-item-email.olark-survey-form-is-required > input")
    WebElement emailInput;

    @FindBy(how = How.CSS, using = "#olark-custom-survey-form > div > div > div.olark-survey-form-item.olark-survey-form-item-textarea.olark-survey-form-is-required > textarea")
    WebElement messageInput;

    @FindBy(how = How.CSS, using = "#olark-custom-survey-form > div > div > button")
    WebElement sendButton;

    @FindBy(how = How.CSS, using = "#olark-wrapper > div.olark-launch-button-wrapper > div > button")
    WebElement closeButton;

    @FindBy(how = How.ID, using = "olark-survey-form-error-msg")
    WebElement fieldsError;

    @FindBy(how = How.CLASS_NAME, using = "olark-thank-you-component")
    WebElement contactSuccess;

    public ContactFormPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void contactForm(String name, String email, String message) {
        wait.until(ExpectedConditions.elementToBeClickable(contactButton));
        contactButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(sendButton));
        nameInput.click();
        nameInput.clear();
        nameInput.sendKeys(name);
        emailInput.click();
        emailInput.clear();
        emailInput.sendKeys(email);
        messageInput.click();
        messageInput.clear();
        messageInput.sendKeys(message);
        sendButton.click();
    }

    public void clearContactForm() {
        nameInput.click();
        nameInput.sendKeys("a");
        nameInput.clear();
        emailInput.click();
        emailInput.sendKeys("a");
        emailInput.clear();
        messageInput.click();
        messageInput.sendKeys("a");
        messageInput.clear();
        closeButton.click();
    }

    public boolean checkErr(String error, Errors type) {
        try {
            if (OtherUtils.checkElementPresent(fieldsError)) {
                switch (type) {
                    case fieldsError:
                        return error.equals(fieldsError.getText());
                    case emailError:{
                        return error.equals(fieldsError.getText());}
                    default:
                        return false;
                }
            }
        } catch (NoSuchElementException e) {
            return true;
        }
        return false;
    }

    public boolean checkNotification(String notification, Notifications type) {

        if (OtherUtils.checkMessagePresentOnElement(contactSuccess, notification))
            return true;

        switch (type) {
            case contactSuccess:
                return notification.equals(contactSuccess.getText());
            default:
                return false;
        }
    }

    public void openContactPage(String hostname) {
        System.out.println("Open the next url:" + hostname);
        driver.get(hostname);
    }

}