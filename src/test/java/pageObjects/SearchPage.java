package pageObjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchPage {

    private WebDriver driver;
    WebDriverWait wait;

    @FindBy(how = How.ID, using = "header_search_form_input")
    WebElement searchInput;

    public SearchPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void register(String searchWord){
        searchInput.click();
        searchInput.clear();
        searchInput.sendKeys(searchWord);
        searchInput.sendKeys(Keys.ENTER);
    }

    public void openMainPage(String hostname) {
        System.out.println("Open the next url:" + hostname);
        driver.get(hostname);
    }

}
