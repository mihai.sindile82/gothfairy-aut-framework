package pageObjects;

import utils.Errors;
import utils.Notifications;
import utils.OtherUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
    private WebDriver driver;
    WebDriverWait wait;

    @FindBy(how = How.ID, using = "profile_email")
    WebElement usernameInput;

    @FindBy(how = How.ID, using = "profile_password")
    WebElement passwordInput;

    @FindBy(how = How.CSS, using = "#page_login_form > div:nth-child(4) > button")
    WebElement submitButton;

    @FindBy(how = How.ID, using = "parsley-id-5")
    WebElement userError;

    @FindBy(how = How.CLASS_NAME, using = "parsley-type")
    WebElement emailError;

    @FindBy(how = How.ID, using = "parsley-id-7")
    WebElement passwordError;

    @FindBy(how = How.ID, using = "message_profile_password")
    WebElement passwordCondError;

    @FindBy(how = How.ID, using = "message_password_mismatch")
    WebElement generalError;

    @FindBy(how = How.CLASS_NAME, using = "title-primary")
    WebElement loginSuccess;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void login(String username, String password) {
        usernameInput.clear();
        usernameInput.sendKeys(username);
        passwordInput.clear();
        passwordInput.sendKeys(password);
        submitButton.submit();
    }

    public boolean checkErr(String error, Errors type) {

        if (OtherUtils.checkMessagePresentOnElement(userError, error))
            return true;
        if (OtherUtils.checkMessagePresentOnElement(passwordError, error))
            return true;
        if (OtherUtils.checkMessagePresentOnElement(passwordCondError, error))
            return true;
        if (OtherUtils.checkMessagePresentOnElement(emailError, error))
            return true;
        if (OtherUtils.checkMessagePresentOnElement(generalError, error))
            return true;

        switch (type) {
            case userError:
                return error.equals(userError.getText());
            case passwordError:
                return error.equals(passwordError.getText());
            case passwordCondError:
                return error.equals(passwordCondError.getText());
            case emailError:
                return error.equals(emailError.getText());
            case generalError:
                return error.equals(generalError.getText());
            default:
                return false;
        }
    }

    public boolean checkNotification(String notification, Notifications type) {

        if (OtherUtils.checkMessagePresentOnElement(loginSuccess, notification))
            return true;

        switch (type) {
            case loginSuccess:
                String full = loginSuccess.getText();
                String trim = full.substring(0, full.indexOf(","));
                System.out.println(trim);
                return notification.equals(trim);
            default:
                return false;
        }
    }

    public void openLoginPage(String hostname) {
        System.out.println("Open the next url:" + hostname + "login");
        driver.get(hostname + "login");
    }
}
