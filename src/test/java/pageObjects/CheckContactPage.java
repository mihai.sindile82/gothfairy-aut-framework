package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Elements;

public class CheckContactPage {

    private WebDriver driver;
    WebDriverWait wait;

    @FindBy(how = How.CSS, using = "#page_content > div.row > div.col-12.col-lg-4.mt-5.mt-lg-0 > h1")
    WebElement firmName;

    @FindBy(how = How.CSS, using = "#page_content > div.row > div.col-12.col-lg-4.mt-5.mt-lg-0 > p")
    WebElement firmInfo;

    @FindBy(how = How.CSS, using = "#page_content > div.row > div.col-12.col-lg-4.mt-5.mt-lg-0 > ul > li:nth-child(1) > div > div")
    WebElement firmAddress;

    @FindBy(how = How.CSS, using = "#page_content > div.row > div.col-12.col-lg-4.mt-5.mt-lg-0 > ul > li:nth-child(2) > div.list-content > div.list-desc.link-inherit.off-on > span > a")
    WebElement firmEmail;

    @FindBy(how = How.CSS, using = "#page_content > div.row > div.col-12.col-lg-4.mt-5.mt-lg-0 > ul > li:nth-child(3) > div.list-content > div.list-desc > span.d-none.d-lg-inline")
    WebElement firmPhone;

    public CheckContactPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public String contactFlow(Elements type) {
        switch (type) {
            case firmName:
                return firmName.getText();
            case firmInfo:
                return firmInfo.getText();
            case firmAddress:
                return firmAddress.getText();
            case firmEmail:
                return firmEmail.getText();
            case firmPhone:
                return firmPhone.getText();
            default:
                return "";
        }

    }

    public void openContactPage(String hostname) {
        System.out.println("Open the next url:" + hostname + "contact");
        driver.get(hostname + "contact");
    }

}
