package pageObjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProfilePage {
    private WebDriver driver;
    WebDriverWait wait;

    @FindBy(how = How.LINK_TEXT, using = "Iesire")
    WebElement logoutMenu;

    @FindBy(how = How.CLASS_NAME, using = "header-logo-img")
    WebElement logo;

    public ProfilePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void logoutAction(){
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", logoutMenu);
    }

    public boolean logout() {
        wait.until(ExpectedConditions.elementToBeClickable(logo));

        try{
        if (driver.findElement(By.linkText("Iesire")).isDisplayed()) return false;
        } catch (NoSuchElementException e) {
            return true;
        }
        return false;
    }

}
