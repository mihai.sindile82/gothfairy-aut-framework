package pageObjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.OtherUtils;

import static utils.OtherUtils.priceConverter;

public class ShopPage {

    private WebDriver driver;
    WebDriverWait wait;

    @FindBy(how = How.LINK_TEXT, using = "Agendă / Jurnal cu coperți în relief Cartea vrajilor")
    WebElement firstProduct;

    @FindBy(how = How.ID, using = "prod_price_2063")
    WebElement productPriceInitial;

    @FindBy(how = How.CSS, using = "#page_cart_preview_form > div.table-responsive > table > tbody > tr > td.text-right.text-nowrap.pl-0.pl-lg-3.col-narrow > p")
    WebElement productPriceInCart;

    @FindBy(how = How.ID, using = "btn_cart_main_2063")
    WebElement addToCart;

    @FindBy(how = How.XPATH, using = "//*[@id=\"page_cart_preview_form\"]/div[1]/table/tbody/tr/td[2]/p[1]/span/a[2]")
    WebElement plusButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"page_cart_preview_form\"]/div[1]/table/tbody/tr/td[2]/p[1]/span/a[1]")
    WebElement minusButton;

    @FindBy(how = How.CLASS_NAME, using = "price-shipping")
    WebElement shippingPrice;

    @FindBy(how = How.CLASS_NAME, using = "text-muted")
    WebElement emptyCart;

    @FindBy(how = How.CLASS_NAME, using = "price-total")
    WebElement totalPrice;

    @FindBy(how = How.XPATH, using = "//*[@id=\"page_cart_preview_form\"]/div[1]/table/tbody/tr/td[2]/p[1]/span/input")
    WebElement quantity;

    @FindBy(how = How.CSS, using = "#fancybox-container-1 > div.fancybox-inner > div.fancybox-stage > div > div > div > div.fancy-footer > a.btn.btn-primary.float-right.d-none.d-lg-inline-block")
    WebElement sendOrder;

    @FindBy(how = How.CLASS_NAME, using = "prod-price")
    WebElement productPriceInCheckout;

    @FindBy(how = How.CSS, using = "#olark-wrapper > div.olark-launch-button-wrapper.olark-text-button > div > button")
    WebElement contactButton;

    public ShopPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void goToProduct(){
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", firstProduct);
    }

    public void addToCart() {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        wait.until(ExpectedConditions.elementToBeClickable(addToCart));
        jse.executeScript("arguments[0].click()", addToCart);
    }

    public void addPlusProduct(){
        wait.until(ExpectedConditions.visibilityOf(sendOrder));
        String initial = productPriceInCart.getText();
        plusButton.click();
        wait.until(ExpectedConditions.not(ExpectedConditions.textToBePresentInElement(productPriceInCart,initial)));
    }

    public void removeMinusProduct(){
        wait.until(ExpectedConditions.visibilityOf(sendOrder));
        int number = numberOfProducts();
        for (int i = 1; i < number; i++)
        {
            String initial = productPriceInCart.getText();
            minusButton.click();
            wait.until(ExpectedConditions.not(ExpectedConditions.textToBePresentInElement(productPriceInCart,initial)));
        }
        minusButton.click();
    }

    public void goToCheckOut(){
        sendOrder.click();
        wait.until(ExpectedConditions.elementToBeClickable(contactButton));
    }


    public float productPriceInitial() {
        return priceConverter(productPriceInitial.getText());
    }

    public float productPriceInCart() {
        wait.until(ExpectedConditions.visibilityOf(sendOrder));
        return priceConverter(productPriceInCart.getText());
    }

    public float productPriceInCheckout() {
        wait.until(ExpectedConditions.visibilityOf(contactButton));
        return priceConverter(productPriceInCheckout.getText());

    }

    public float totalPrice() {
        wait.until(ExpectedConditions.visibilityOf(contactButton));
        return priceConverter(totalPrice.getText());
    }

    public float shippingPrice(){
        wait.until(ExpectedConditions.visibilityOf(contactButton));
        return priceConverter(shippingPrice.getText());
    }

    public boolean checkNoItems(){
        wait.until(ExpectedConditions.visibilityOf(emptyCart));
        return OtherUtils.checkElementPresent(emptyCart);
    }

    public int numberOfProducts(){
        return Integer.parseInt(quantity.getAttribute("value"));
    }

    public void openCelticPage(String hostname, String hostpage) {
        System.out.println("Open the next url:" + hostname + hostpage);
        driver.get(hostname + hostpage);
    }

}
