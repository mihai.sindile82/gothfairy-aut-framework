package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.OtherUtils;

public class MainPage {

    private WebDriver driver;
    WebDriverWait wait;

    @FindBy(how = How.CSS, using = "body > div.upsell-message.upsell-message-15.upsell-message-empty.text-md > div > p > span")
    WebElement promoText;

    @FindBy(how = How.CLASS_NAME, using = "nav nav-footer-top-brands")
    WebElement topBrands;

    public MainPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public String promoElement(){
        return promoText.getText();
    }

    public boolean checkExistance(){
        return OtherUtils.checkElementPresent(topBrands);
    }

    public void openMainPage(String hostname) {
        System.out.println("Open the next url:" + hostname);
        driver.get(hostname);
    }

}
