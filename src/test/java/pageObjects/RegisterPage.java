package pageObjects;

import org.openqa.selenium.By;
import utils.Errors;
import utils.Notifications;
import utils.OtherUtils;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import static utils.OtherUtils.retryingFindClick;

public class RegisterPage {

    private final WebDriver driver;
    WebDriverWait wait;

    @FindBy(how = How.ID, using = "profile_email")
    WebElement userNameInput;

    @FindBy(how = How.ID, using = "profile_password")
    WebElement passwordInput;

    @FindBy(how = How.ID, using = "profile_billing_person_name")
    WebElement nameInput;

    @FindBy(how = How.ID, using = "profile_billing_country")
    WebElement countrySelect;

    @FindBy(how = How.ID, using = "profile_billing_state")
    WebElement countySelect;

    @FindBy(how = How.ID, using = "profile_billing_city")
    WebElement cityInput;

    @FindBy(how = How.ID, using = "profile_billing_address")
    WebElement addressInput;

    @FindBy(how = How.ID, using = "profile_billing_phone")
    WebElement telephoneInput;

    @FindBy(how = How.CSS, using = "#user_account_fields > div.row-newsletter-terms-submit > div.form-group.row-confirm-terms > div > div > label")
    WebElement checkBox;

    @FindBy(how = How.CSS, using = "#user_account_fields > div.row-newsletter-terms-submit > button")
    WebElement submitButton;

    @FindBy(how = How.ID, using = "parsley-id-5")
    WebElement userError;

    @FindBy(how = How.ID, using = "parsley-id-7")
    WebElement passwordError;

    @FindBy(how = How.ID, using = "parsley-id-14")
    WebElement nameError;

    @FindBy(how = How.ID, using = "parsley-id-16")
    WebElement countryError;

    @FindBy(how = How.ID, using = "parsley-id-18")
    WebElement countyError;

    @FindBy(how = How.ID, using = "parsley-id-20")
    WebElement cityError;

    @FindBy(how = How.ID, using = "parsley-id-22")
    WebElement addressError;

    @FindBy(how = How.ID, using = "parsley-id-26")
    WebElement phoneError;

    @FindBy(how = How.CLASS_NAME, using = "parsley-errors-list filled")
    WebElement emailError;

    @FindBy(how = How.CLASS_NAME, using = "parsley-pattern")
    WebElement phoneCondError;

    @FindBy(how = How.CSS, using = "#user_account_fields > div.row-newsletter-terms-submit > div.form-group.row-confirm-terms.text-danger-group > div > div")
    WebElement checkBoxError;

    @FindBy(how = How.ID, using = "message_profile_password")
    WebElement passwordCondError;

    @FindBy(how = How.ID, using = "message_profile_email")
    WebElement emailExistsError;

    @FindBy(how = How.ID, using = "message_register_success")
    WebElement registrationSuccess;

    public RegisterPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void register(String userName, String password, String name, Integer countryIndex, Integer countyIndex,
                         String city, String address, Integer isChecked, String telephone) {
        wait.until(ExpectedConditions.elementToBeClickable(userNameInput));
        userNameInput.click();
        userNameInput.clear();
        userNameInput.sendKeys(userName);
        passwordInput.click();
        passwordInput.clear();
        passwordInput.sendKeys(password);
        nameInput.click();
        nameInput.clear();
        nameInput.sendKeys(name);
        wait.until(ExpectedConditions.presenceOfNestedElementsLocatedBy(By.id("profile_billing_country"), By.tagName("option")));
        Select dropdownCountry = new Select(countrySelect);
        if (retryingFindClick(countrySelect))
        dropdownCountry.selectByIndex(countryIndex);
        wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.presenceOfNestedElementsLocatedBy(By.id("profile_billing_state"), By.tagName("option")));
        Select dropdownCounty = new Select(countySelect);
        if (retryingFindClick(countySelect))
        dropdownCounty.selectByIndex(countyIndex);
        cityInput.click();
        cityInput.clear();
        cityInput.sendKeys(city);
        addressInput.click();
        addressInput.clear();
        addressInput.sendKeys(address);
        telephoneInput.click();
        telephoneInput.clear();
        telephoneInput.sendKeys(telephone);
        if (isChecked == 1) checkBox.click();
        submitButton.submit();
    }

    public boolean checkErr(String error, Errors type) {

        if (OtherUtils.checkMessagePresentOnElement(userError, error))
            return true;
        if (OtherUtils.checkMessagePresentOnElement(passwordError, error))
            return true;
        if (OtherUtils.checkMessagePresentOnElement(nameError, error))
            return true;
        if (OtherUtils.checkMessagePresentOnElement(countryError, error))
            return true;
        if (OtherUtils.checkMessagePresentOnElement(countyError, error))
            return true;
        if (OtherUtils.checkMessagePresentOnElement(cityError, error))
            return true;
        if (OtherUtils.checkMessagePresentOnElement(addressError, error))
            return true;
        if (OtherUtils.checkMessagePresentOnElement(phoneError, error))
            return true;
        if (OtherUtils.checkMessagePresentOnElement(emailError, error))
            return true;
        if (OtherUtils.checkMessagePresentOnElement(phoneCondError, error))
            return true;
        if (OtherUtils.checkMessagePresentOnElement(phoneCondError, error))
            return true;
        if (OtherUtils.checkMessagePresentOnElement(checkBoxError, error))
            return true;
        if (OtherUtils.checkMessagePresentOnElement(passwordCondError, error))
            return true;
        if (OtherUtils.checkMessagePresentOnElement(emailExistsError, error))
            return true;

        switch (type) {
            case userError:
                return error.equals(userError.getText());
            case passwordError:
                return error.equals(passwordError.getText());
            case nameError:
                return error.equals(nameError.getText());
            case countryError:
                return error.equals(countryError.getText());
            case countyError:
                return error.equals(countyError.getText());
            case cityError:
                return error.equals(cityError.getText());
            case addressError:
                return error.equals(addressError.getText());
            case phoneError:
                return error.equals(phoneError.getText());
            case emailError:
                return error.equals(emailError.getText());
            case phoneCondError:
                return error.equals(phoneCondError.getText());
            case checkBoxError:
                return error.equals(checkBoxError.getAttribute("class"));
            case passwordCondError:
                return error.equals(passwordCondError.getText());
            case emailExistsError:
                return error.equals(emailExistsError.getText());
            default:
                return false;
        }
    }

    public boolean checkNotification(String notification, Notifications type) {

        if (OtherUtils.checkMessagePresentOnElement(registrationSuccess, notification))
            return true;

        switch (type) {
            case registrationSuccess:
                return notification.equals(registrationSuccess.getText());
            default:
                return false;
        }
    }

    public void openRegisterPage(String hostname) {
        System.out.println("Open the next url:" + hostname + "inregistrare");
        driver.get(hostname + "inregistrare");
    }
}
