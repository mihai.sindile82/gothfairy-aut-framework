package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FavouritePage {

    private WebDriver driver;
    WebDriverWait wait;

    @FindBy(how = How.CSS, using = "#page_content > div.block-products-catalog > div > div.prod-box.d-flex.prod-242.prod-cat-20.prod-in-stock.prod-discounted > div > div > div.prod-actions > button")
    WebElement addToFavourite;

    @FindBy(how = How.CSS, using = "#page_content > div.block-products-catalog > div > div > div > div > div.prod-actions > button")
    WebElement removeFromFavourite;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Produse favorite")
    WebElement favouriteLink;

    @FindBy(how = How.CSS, using = "#olark-wrapper > div.olark-launch-button-wrapper.olark-text-button > div > button")
    WebElement contactButton;

    public FavouritePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void addToFavourite() {

        JavascriptExecutor jse = (JavascriptExecutor) driver;
        wait.until(ExpectedConditions.elementToBeClickable(addToFavourite));
        jse.executeScript("arguments[0].click()", addToFavourite);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#page_content > div.block-products-catalog > div > div > div > div > div.prod-actions > button")));
        jse.executeScript("arguments[0].click()", favouriteLink);
        wait.until(ExpectedConditions.elementToBeClickable(contactButton));
    }

    public void removeFavouriteFlow() {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        wait.until(ExpectedConditions.elementToBeClickable(removeFromFavourite));
        jse.executeScript("arguments[0].click()", removeFromFavourite);
    }

    public void openCelticPage(String hostname, String hostpage) {
        System.out.println("Open the next url:" + hostname + hostpage);
        driver.get(hostname + hostpage);
    }
}
