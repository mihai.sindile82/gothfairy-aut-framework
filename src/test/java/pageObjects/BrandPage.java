package pageObjects;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Filters;

public class BrandPage {
    private WebDriver driver;
    WebDriverWait wait;

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'STAR WARS')]")
    WebElement brandStarWars;

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Marvel')]")
    WebElement brandMarvel;

    @FindBy(how = How.CSS, using = "#sidebar_box_product_filters_content > div.filter-block.filter-categorii.filter-type-cat.mobile-filter-block > div.filter-title.mobile-filter-title.title-collapsible.collapsed")
    WebElement category;

    @FindBy(how = How.CSS, using = "#sidebar_box_cat_cat_content > div > ul > li.filter-option.option-stil > div > a")
    WebElement styleCheckbox;

    @FindBy(how = How.CSS, using = "#sidebar_box_cat_cat_content > div > ul > li.filter-option.option-stil > div > a > span")
    WebElement filterNumber;


    public BrandPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public WebElement brandFilter(Filters brand) {
        switch (brand) {
            case StarWars:
                return brandStarWars;
            case Marvel:
                return brandMarvel;
            default:
                return null;
        }
    }

    public int brandClickFilter(Filters brand) {

        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", brandFilter(brand));
        category.click();
        int count = Integer.parseInt(filterNumber.getAttribute("data-cnt"));
        jse.executeScript("arguments[0].click()", styleCheckbox);
        return count;
    }

    public void openBrandPage(String hostname) {
        System.out.println("Open the next url:" + hostname + "brands");
        driver.get(hostname + "brands");
    }

}
